'use strict'


    //Programming not recommended, just for teaching only
var movies = require('../models/movies');
var express = require('express');
var router = express.Router();

router
    .use(movies)
    //Root route
    .get('/', function(req, res, next) {
        req.getConnection(function (err, movies) {
            movies.query('SELECT * FROM movie',  function (err, rows) {
            var locals = {
                tittle : 'Lista de películas',
                data : rows
            }

            res.render('index', locals)
            })
        })
    })

    //Add route
    .get('/agregar', function(req, res, next) {
        res.render('add_movie', {title: "Agregar Pelicula"});
    })

    .post('/', function (req, res, next) {
        req.getConnection(function (err, movies) {

            var movie = {
                movie_id: 4,
                title: req.body.title,
                release_year : req.body.release_year,
                rating: req.body.rating,
                image: req.body.image
            }

            console.log(movie);

            movies.query('INSERT INTO movie SET ? ', movie, function (err, rows) {
            return (err) ? res.redirect('/agregar'): res.redirect('/');

            })
        })
    })

    .get('/editar/:movie_id', function (req, res, next) {
        var movie_id = req.params.movie_id;

        console.log(movie_id);

        req.getConnection(function (err, movies) {
            movies.query('SELECT * FROM movie WHERE movie_id = ?', movie_id, function (err, rows) {
                if(err){
                    throw(err);
                }else{
                    var locals = {
                        title : 'Editar Pelicula',
                        data: rows
                    }

                    res.render('edit_movie', locals);
                }
            });
        })
    })
    
    .post('/actualizar/:movie_id', function (req, res, next) {
        req.getConnection(function (err, movies) {

            var movie = {
                movie_id: 4,
                title: req.body.title,
                release_year : req.body.release_year,
                rating: req.body.rating,
                image: req.body.image
            }

            console.log(movie);

            movies.query('UPDATE  movie SET ? WHERE movie_id = ?', [movie, movie.movie_id], function (err, rows) {
                return (err) ? res.redirect('/editar'): res.redirect('/');

            })
        })
    })

    .post('/eliminar/:movie_id', function (req, res, next) {

        var movie_id = req.params.movie_id;

        req.getConnection(function (err, movies) {
            movies.query('DELETE FROM movie WHERE movie_id = ?', movie_id, function (err, rows) {
                return (err) ? next(new Error('Registro no encontrado')) : res.redirect('/');
            });
        })
    })

module.exports = router;
