var express = require('express');
var logger = require('morgan');
var path = require("path");
var routes = require('./routes/index');
var bodyParser = require('body-parser');
var port = (process.env.port || 3000);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('port', port);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(routes);



module.exports = app;
