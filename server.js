var app = require('./app'),
    server = app.listen(app.get('port'), function () {
        console.log('Starting Express on port ' + app.get('port'));
    });